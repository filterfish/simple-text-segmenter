#!/usr/bin/env ruby
# coding: utf-8

require 'pp'
require 'pathname'
require 'multi_json'

class Segmenter

  BLANK_LINE_THRESHOLD = 5

  MIN_SEGMENT_SIZE = 200

  MIN_CHUNK_SIZE = 800

  MIN_DENSITY = 0.7
  MIN_NON_SPACE_LENGTH = 10
  PUNTUATION_RATIO = 0.9
  MIN_NON_WORD_RATIO = 0.4
  MIN_WORD_COUNT = 2


  def initialize(document, dict, debug=true)
    @debug = debug
    @dict = dict
    @lines_with_stats = generate_line_stats(document)
    @processed_lines = []
    @chunks = []
  end

  def process
    @lines_with_stats.select do |line_with_stats|
      if keep?(line_with_stats)
        @processed_lines << pr(line_with_stats)
      else
        @processed_lines << indented_pr(line_with_stats)
      end
    end
  end

  def concatenate_short_chunks
    intermediate_chunk = []

    @chunks = @chunks.reduce([]) do |acc, chunk|
      n = section_length(chunk)
      if n < MIN_CHUNK_SIZE
        intermediate_chunk = intermediate_chunk + chunk
      else
        acc << intermediate_chunk if !intermediate_chunk.empty?
        acc << chunk
        intermediate_chunk = []
      end
      acc
    end

    @chunks << intermediate_chunk if !intermediate_chunk.empty?
  end

  def blank_line_count(all, n)
    count = 0

    while all[n + count] && all[n + count].empty? do
      count += 1
    end
    count
  end

  def print_output
    @concatenated_lines.each {|l| puts l }
  end


  def segment
    nl = @processed_lines.length
    n = 0
    section = []

    while n < nl do
      line = @processed_lines[n]

      if line.empty?
        bl_count = blank_line_count(@processed_lines, n)

        if bl_count < BLANK_LINE_THRESHOLD
          section << line
          n += 1
        else

          len = section_length(section)

          if len > MIN_SEGMENT_SIZE
            @chunks << section
          end

          section = []
          n += bl_count
        end
      else
        section << line
        n += 1
      end
    end
  end

  def export_segments(prefix, io: STDOUT)
    json = @chunks.length.times.zip(@chunks).each_with_object({}) do |(n, e), acc|
      acc["#{prefix}/#{n}"] = e.join('')
    end

    {:content => json}
  end

  def export_segments_as_json(prefix, io: STDOUT)
    json = @chunks.length.times.zip(@chunks).each_with_object({}) do |(n, e), acc|
      acc["#{prefix}/#{n}"] = e.join('')
    end

    io.puts MultiJson.dump(:content => json)
  end

  private

  # This is the main method that checks if the line should be kept or not. It
  # is completely ad-hoc and almost certainly will require tweaking if your
  # documents are not the same as mine!
  # TODO: parameterise this.
  def keep?(l)
    l[:rstriped_density]    > MIN_DENSITY           &&
    l[:length_non_space]    > MIN_NON_SPACE_LENGTH  &&
    l[:punct_ratio]         > PUNTUATION_RATIO      &&
    l[:dict_word_count]     > MIN_WORD_COUNT        &&
    l[:word_non_word_ratio] < MIN_NON_WORD_RATIO
  end

  def section_length(s)
    s.map(&:strip).join.length
  end

  # Print a line with debug or not depending on the second argument
  def pr(l)
    (@debug) ? line_with_density(l) : line(l)
  end

  # Print a line with debug or not depending on the second argument
  def indented_pr(l)
    (@debug) ? indented_line_with_density(l) : ""
  end

  def line(line_with_stats)
    sprintf "%s\n", line_with_stats[:line]
  end

  def line_with_density(line_with_stats)
    sprintf "%0.2f, %0.2f [%0.2f] - %s\n", line_with_stats[:rstriped_density], line_with_stats[:punct_ratio], line_with_stats[:word_non_word_ratio], line_with_stats[:line]
  end

  def indented_line_with_density(line_with_stats)
    sprintf "%0.2f, %0.2f [%0.2f] -               >> %s <<\n", line_with_stats[:rstriped_density], line_with_stats[:punct_ratio], line_with_stats[:word_non_word_ratio], line_with_stats[:line]
  end

  def generate_line_stats(document)
    document.readlines.map do |l|
      line = l.sub("\n", '')
      rstripped_line = line.rstrip
      tokenised_line = tokenise(line)

      length = line.length
      rstriped_length = rstripped_line.length
      word_count = tokenised_line.length

      dict_word_count = tokenised_line.inject(0) {|count, word| @dict.lookup(word, count) }

      word_non_word_ratio = ratio(word_count - dict_word_count, word_count)

      length_non_space = line.gsub(' ', '').length
      density = ratio(length_non_space, length)
      rstriped_density = ratio(length_non_space, rstriped_length)
      punct_ratio = ratio(line.gsub(/[[:punct:]]/, '').length, length)

      {:line => line, :length => length, :length_non_space => length_non_space, :rstriped_length => rstriped_length,
       :density => density, :rstriped_density => rstriped_density, :word_non_word_ratio => word_non_word_ratio,
       :punct_ratio => punct_ratio, :word_count => word_count, :dict_word_count => dict_word_count}
    end
  end

  def tokenise(line)
    line.downcase.split(/[ ,\(\)\.-]/)
  end

  def ratio(a, b)
    (a == 0) ? 0 : a.to_f / b.to_f
  end
end
  # -------------------------------------------

class Dictionary

  DEFAULT_DICT_PATH = "/usr/share/dict/words"

  # Read the dictionary file and add it to a Hash
  def initialize(path=DEFAULT_DICT_PATH)
    @dict = Pathname.new(path).readlines.each_with_object({}) {|w, acc| acc[w.strip.downcase] = true }
  end

  # Look up a word and increment a counter if supplied otherwise it returns 1
  def lookup(w, count=0)
    (count + ((is_word?(w) || is_num?(w)) ? 1 : 0))
  end

  private

  def is_word?(w)
    @dict[w]
  end

  def is_num?(w)
    w.sub(/[[:alpha:]]+/, '').match(/^[[:digit:]]+$/)
  end
end



file_paths = ARGV

if file_paths.empty?
  puts "usage: #{$0} <paths>"
  exit 1
end


dictionary = Dictionary.new

data = file_paths.map do |path_name|
  STDERR.puts "Processing: #{path_name}"

  path = Pathname.new(path_name)

  segmenter = Segmenter.new(path, dictionary, false)

  segmenter.process
  segmenter.segment
  segmenter.concatenate_short_chunks
  segmenter.export_segments(path.basename(path.extname))
end

merged_data = data.inject({}) do |acc, e|
  acc.merge(e[:content])
end

puts MultiJson.dump(:content => merged_data)
